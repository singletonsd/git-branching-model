#!/usr/bin/env bash
DIR__=""
if [ ! -d Scripts ]; then
  DIR__="/opt/git-branching-model/"
fi
source ${DIR__}Scripts/Common/variables.sh
source ${DIR__}Scripts/Common/functions.sh
if [ -z "$(whereis curl | cut -d ':' -f2)" ]; then
    echo -e "${B_RED}CURL is not installed ${RESET}"
    echo -e "${B_CYAN}Installing CURL...${RESET}"
    sudo apt-get install curl
fi
if [ -z "$(whereis jq | cut -d ':' -f2)" ]; then
    echo -e "${B_RED}JQ is not installed ${RESET}"
    echo -e "${B_CYAN}Installing JQ...${RESET}"
    sudo apt-get install jq
fi
TAGS="$(curl https://gitlab.com/api/v4/projects/4348667/repository/tags)"
LAST_VERSION="$(echo ${TAGS} | jq -r ".[] | .name" | sed -n '1p' | cut -d 'v' -f2)"
CURRENT_VERSION="$(cat "${DIR__}VERSION")"
URL_DOWNLOAD=https://gitlab.com/singleton.com.ar/git-branching-model/repository/v${LAST_VERSION}/archive.zip
echo -e "${B_CYAN} LAST VERSION FOUND iS ${LAST_VERSION}.${RESET}"
echo -e "${B_CYAN} CURRENT VERSION iS ${CURRENT_VERSION}. ${RESET}"
BASE=(`echo $CURRENT_VERSION | tr '.' ' '`)
V_C_MAJOR=${BASE[0]}
V_C_MINOR=${BASE[1]}
V_C_PATCH=${BASE[2]}
BASE=(`echo $LAST_VERSION | tr '.' ' '`)
V_L_MAJOR=${BASE[0]}
V_L_MINOR=${BASE[1]}
V_L_PATCH=${BASE[2]}
if [ ${V_L_MAJOR} -gt ${V_C_MAJOR} ]; then
    downloadAndInstall
else
    if [ ${V_L_MINOR} -gt ${V_C_MINOR} ]; then
        downloadAndInstall
    else
        if [ ${V_L_PATCH} -gt ${V_C_PATCH} ]; then
            downloadAndInstall
        else
            echo -e "${B_GREEN}Binary is upto date.${RESET}"
        fi
    fi
fi