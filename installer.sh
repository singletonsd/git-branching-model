#!/usr/bin/env bash
source Scripts/Common/variables.sh
LAST_VERSION="$(cat VERSION)"
echo -e "${B_GREEN}Singleton Git Branching Model. v$LAST_VERSION INSTALLER${RESET}"
if [ -f /opt/git-branching-model/VERSION ]; then 
  echo -e "${B_YELLOW}Existing binary found. Cheking version...${RESET}"
  CURRENT_VERSION="$(cat /opt/git-branching-model/VERSION)"
  echo -e "${B_YELLOW}Binary version is v$CURRENT_VERSION${RESET}"
  LAST_=(`echo $LAST_VERSION | tr -d '.'`)
  CURRENT_=(`echo $CURRENT_VERSION | tr -d '.'`)
  if [ "$LAST_" -gt "$CURRENT_" ]; then
    echo -e "${B_YELLOW}Intalled version is old. Deleting...${RESET}"
    sudo rm -r /opt/git-branching-model    
  fi  
fi
if [ ! -d /opt/git-branching-model ]; then
  echo -e "${BLUE}Copying files...${RESET}"
  sudo cp -R ../git-branching-model /opt
  sudo rm /opt/git-branching-model/installer.sh
else
  echo -e "${B_GREEN}Binary was already installed.${RESET}"
fi
if [ ! -f /usr/local/bin/branching-model ]; then
  echo -e "${GREEN}Creating links...${RESET}"
  sudo ln -s /opt/git-branching-model/branching-model /usr/local/bin/branching-model
fi
echo -e "${B_GREEN}DONE!!${RESET}"
branching-model --help