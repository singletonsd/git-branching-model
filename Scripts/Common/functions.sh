#!/usr/bin/env bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $DIR/variables.sh
function getSecound(){
  case "$2" in
    [Nn]* | -[Nn]*) 
      echo -e "New"
      break
      ;;
    [Ff]* | -[Ff]*) 
      echo -e "Finish"
      break
      ;;
    *) 
      echo -e "${B_RED}Error: Must provide N(n)ew or F(f)inish as parameter for $1 ${RESET}"
      exit 0
      ;;
  esac
}

function usage {
  echo "${B_BLUE}SINGLETON - Git Branching Model - v$(cat $DIR/../../VERSION)${RESET}"
  echo "${B_GREEN} -h,    --[Hh]elp          help.${RESET}"
  echo "${B_GREEN} -[Vv], --[Vv]ersion       version.${RESET}"
  echo "${B_GREEN} -[U], --[Uu]pdate        update binary.${RESET}"
  echo "${B_GREEN} -[u], --[Uu]nistall        unistall binary.${RESET}"
  echo "${B_GREEN} -[Ff], --[Ff]eature    [n | f]       feature option.${RESET}"
  echo "${GREEN}     [Nn],-[Nn]        Create a new feature branch from develop.${RESET}"
  echo "${GREEN}     [Ff],-[Ff]        Remove branch and merge to develop.${RESET}"
  echo "${B_GREEN} -[Rr], --[Rr]elease       release option.${RESET}"
  echo "${GREEN}    [Nn],-[Nn]        Create a new release branch from develop.${RESET}"
  echo "${GREEN}    [Ff],-[Ff]        Remove branch, merge to master and to develop and create a version tag.${RESET}"
  echo "${B_GREEN} -H,    --[Hh]otfix        hotfix option.${RESET}"
  echo "${GREEN}     [Nn],-[Nn]        Create new branch from release for small and quick changes.${RESET}"
  echo "${GREEN}     [Ff],-[Ff]        Remove branch, merge to master and to develop. Create a new version tag.${RESET}"
  echo "${B_CYAN}  Repository: ${CYAN}https://gitlab.com/singleton.com.ar/git-branching-model.git${RESET}"
  exit 0
}

function downloadAndInstall {
  FOLDER="/home/dev/Documents/"
  ARCHIVE="${FOLDER}branching-model"
  ARCHIVE_NAME="${ARCHIVE}.ZIP"
  echo -e "${B_YELLOW}Binary is outdated, starting to update...${RESET}"
  curl ${URL_DOWNLOAD} -o ${ARCHIVE_NAME} 
  unzip ${ARCHIVE_NAME} -d ${FOLDER} > /dev/null 2>/dev/null
  rm ${ARCHIVE_NAME}
  ZIP_FOLDER=$(ls -d ${FOLDER}*/ | grep "git-branching-model-v${LAST_VERSION}")
  mv ${ZIP_FOLDER}  "${FOLDER}git-branching-model"
  echo -e "${B_CYAN} Backing up old version... ${RESET}"
  sudo mv /opt/git-branching-model /opt/git-branching-model-old
  echo -e "${B_CYAN} Installing new version... ${RESET}"
  sudo cp -R ${FOLDER}git-branching-model /opt
  sudo rm /opt/git-branching-model/installer.sh
  rm -r ${FOLDER}git-branching-model
  echo -e "${B_CYAN} Checking new version... ${RESET}"
  if [ -f /opt/git-branching-model/VERSION ]; then
    if [ "$(cat /opt/git-branching-model/VERSION)" == "${LAST_VERSION}" ]; then
      echo -e "${B_CYAN} Binary is upto date. ${RESET}"
      echo -e "${B_CYAN} Deleting old version... ${RESET}"
      sudo rm -r /opt/git-branching-model-old
    else
      echo -e "${B_RED} ERROR FOUND. Download binary is different from last version. Restoring old version... ${RESET}"
      sudo mv /opt/git-branching-model-old /opt/git-branching-model
      if [ -f /opt/git-branching-model/VERSION ]; then
        if [ "$(cat /opt/git-branching-model/VERSION)" == "${CURRENT_VERSION}" ]; then
          echo -e "${B_CYAN} Binary was succesfully restored to previous version ${B_YELLOW}v${CURRENT_VERSION}. ${RESET}"
        else
          echo -e "${B_RED} ERROR FOUND. Download and install binary from:  ${RESET}"
          echo -e "${B_CYAN}  Repository: ${CYAN}https://gitlab.com/singleton.com.ar/git-branching-model.git${RESET}"
          exit 0
        fi
      else
        echo -e "${B_RED} ERROR FOUND. Download and install binary from:  ${RESET}"
        echo "${B_CYAN}  Repository: ${CYAN}https://gitlab.com/singleton.com.ar/git-branching-model.git${RESET}"
        exit 0
      fi
    fi
  else
    echo -e "${B_RED} ERROR FOUND. Restoring old version... ${RESET}"
    sudo mv /opt/git-branching-model-old /opt/git-branching-model
    if [ -f /opt/git-branching-model/VERSION ]; then
      if [ "$(cat /opt/git-branching-model/VERSION)" == "${CURRENT_VERSION}" ]; then
        echo -e "${B_CYAN} Binary was succesfully restored to previous version ${B_YELLOW}v${CURRENT_VERSION}. ${RESET}"
      else
        echo -e "${B_RED} ERROR FOUND. Download and install binary from:  ${RESET}"
        echo "${B_CYAN}  Repository: ${CYAN}https://gitlab.com/singleton.com.ar/git-branching-model.git${RESET}"
        exit 0
      fi
    else
      echo -e "${B_RED} ERROR FOUND. Download and install binary from:  ${RESET}"
      echo "${B_CYAN}  Repository: ${CYAN}https://gitlab.com/singleton.com.ar/git-branching-model.git${RESET}"
      exit 0
    fi
  fi 
}