#!/usr/bin/env bash
NOW="$(date +'%B %d, %Y')"
RED="\033[0;31m"
GREEN="\033[0;32m"
YELLOW="\033[0;33m"
BLUE="\033[0;34m"
PURPLE="\033[0;35m"
CYAN="\033[0;36m"
WHITE="\033[0;37m"
B_RED="\033[1;31m"
B_GREEN="\033[1;32m"
B_YELLOW="\033[1;33m"
B_BLUE="\033[1;34m"
B_PURPLE="\033[1;35m"
B_CYAN="\033[1;36m"
B_WHITE="\033[1;37m"
RESET="\033[0m"
QUESTION_FLAG="${GREEN}?"
WARNING_FLAG="${YELLOW}!"
NOTICE_FLAG="${CYAN}❯"
__PATH=""
#if [ $# -eq 1 ];then
#  PATH="`dirname \"$1\"`"              # relative
#  PATH="`( cd \"$PATH\" && pwd )`"  # absolutized and normalized  
#else
__PATH="$PWD"
#fi
GIT_DIR="--git-dir=$PATH/.git"    