#!/usr/bin/env bash

# Works with a file called VERSION,
# the contents of which should be a semantic version number
# such as "1.2.3" or even "1.2.3-beta+001.ab"

# this script will display the current version, automatically
# suggest a "minor" version update, and ask for input to use
# the suggestion, or a newly entered value.

# once the new version number is determined, the script will
# pull a list of changes from git history, prepend this to
# a file called CHANGELOG.md (under the title of the new version
# number), give user a chance to review and update the changelist
# manually if needed and create a GIT tag.
DIR__=""
if [ ! -d ../Feature ]; then
  DIR__="/opt/git-branching-model/Scripts/Feature/"
fi
source $DIR__../Common/variables.sh   

LATEST_HASH=`git log --pretty=format:'%h' -n 1`

ADJUSTMENTS_MSG="${QUESTION_FLAG} ${B_CYAN}Commits messages wrote to ${B_WHITE}CHANGELOG.md${B_CYAN}.${RESET}"

if [ -f "$__PATH"/VERSION ]; then
    BASE_STRING=`cat VERSION`
    BASE_LIST=(`echo $BASE_STRING | tr '.' ' '`)
    V_MAJOR=${BASE_LIST[0]}
    V_MINOR=${BASE_LIST[1]}
    V_PATCH=${BASE_LIST[2]}
    echo -e "${NOTICE_FLAG} Current version: ${B_WHITE}$BASE_STRING"
    echo -e "${NOTICE_FLAG} Latest commit hash: ${B_WHITE}$LATEST_HASH"
    while true;
    do
	    case $1 in
		    -M)
			    V_MAJOR=$((V_MAJOR + 1)); V_MINOR=0; V_PATCH=0;
          break ;;
		    -m)
			    V_MINOR=$((V_MINOR + 1));
          V_PATCH=0;
			    break ;;
        -p)
			    V_PATCH=$((V_PATCH + 1));
			    break ;;
		    \?)
			    echo "-M: Major version change M.0.0"
          echo "-m: Minor version change 0.m.0"
          echo "-p: Patch version change 0.0.p"
			    exit
	    esac
    done
    SUGGESTED_VERSION="$V_MAJOR.$V_MINOR.$V_PATCH"
    echo -ne "${QUESTION_FLAG} ${B_CYAN}Enter a version number [${B_WHITE}$SUGGESTED_VERSION${B_CYAN}]: "
    read INPUT_STRING
    if [ "$INPUT_STRING" = "" ]; then
        INPUT_STRING=$SUGGESTED_VERSION
    fi
    echo -e "${NOTICE_FLAG} Will set new version to be ${B_WHITE}$INPUT_STRING"
    echo $INPUT_STRING > VERSION
    echo "## $INPUT_STRING ($NOW)" > tmpfile
    git log --pretty=format:"  - %s" "v$BASE_STRING"...HEAD >> tmpfile
    echo "" >> tmpfile
    echo "" >> tmpfile
    cat CHANGELOG.md >> tmpfile
    mv tmpfile CHANGELOG.md
    echo -e "$ADJUSTMENTS_MSG"
else
    echo -e "${WARNING_FLAG} Could not find a VERSION file."
    echo -ne "${QUESTION_FLAG} ${B_CYAN}Do you want to create a version file and start from scratch? [${B_WHITE}y${B_CYAN}]: "
    read RESPONSE
    if [ "$RESPONSE" = "" ]; then RESPONSE="y"; fi
    if [ "$RESPONSE" = "Y" ]; then RESPONSE="y"; fi
    if [ "$RESPONSE" = "Yes" ]; then RESPONSE="y"; fi
    if [ "$RESPONSE" = "yes" ]; then RESPONSE="y"; fi
    if [ "$RESPONSE" = "YES" ]; then RESPONSE="y"; fi
    if [ "$RESPONSE" = "y" ]; then
        SUGGESTED_VERSION="0.1.0"
        echo -ne "${QUESTION_FLAG} ${B_CYAN}Enter a version number [${B_WHITE}$SUGGESTED_VERSION${B_CYAN}]: "
        read INPUT_STRING
        if [ "$INPUT_STRING" = "" ]; then
            INPUT_STRING=$SUGGESTED_VERSION
        fi
        echo $INPUT_STRING > VERSION
        echo "## $INPUT_STRING ($NOW)" > CHANGELOG.md
        git log --pretty=format:"  - %s" >> CHANGELOG.md
        echo "" >> CHANGELOG.md
        echo "" >> CHANGELOG.md
        echo -e "$ADJUSTMENTS_MSG"
    fi
fi
echo -e "${NOTICE_FLAG} Finished.${RESET}"