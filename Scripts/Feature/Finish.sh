#!/usr/bin/env bash
DIR__=""
if [ ! -d ../Feature ]; then
  DIR__="/opt/git-branching-model/Scripts/Feature/"
fi
source $DIR__../Common/variables.sh   

BRANCH_NAME="$(git branch | grep \* | cut -d ' ' -f2)"

if [ "$BRANCH_NAME" != "develop" ] && [ "$BRANCH_NAME" != "master" ] ; then
  echo -ne "${QUESTION_FLAG} ${CYAN}Do you want to merge the branch $BRANCH_NAME into develop? [${WHITE}y${CYAN}]: ${RESET}"
  read RESPONSE
  if [ "$RESPONSE" = "" ]; then RESPONSE="y"; fi
  if [ "$RESPONSE" = "Y" ]; then RESPONSE="y"; fi
  if [ "$RESPONSE" = "Yes" ]; then RESPONSE="y"; fi
  if [ "$RESPONSE" = "yes" ]; then RESPONSE="y"; fi
  if [ "$RESPONSE" = "YES" ]; then RESPONSE="y"; fi
  if [ "$RESPONSE" = "y" ]; then
    git $FLAGS checkout develop
    git $FLAGS merge --no-ff $BRANCH_NAME -m "Merging branch "$BRANCH_NAME" into develop branch."
    if [ "$(git branch -r | cut -d '/' -f2 | grep feature )" == "$BRANCH_NAME" ] ; then
      git push origin --delete $BRANCH_NAME     
    fi    
    git branch -D $BRANCH_NAME
    if [ $(git remote -v | wc -l) == 0 ] ; then
      echo -e "${B_RED}There is no remote repository configurated. Please add it and then push develop branch.${RESET}"    
    else    
      git push origin develop 
    fi
  fi
else
  echo -e "${B_RED}You cannot merge branch " $BRANCH_NAME "to develop branch.${RESET}"
  exit 1
fi