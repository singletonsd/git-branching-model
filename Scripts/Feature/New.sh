#!/usr/bin/env bash
DIR__=""
if [ ! -d ../Feature ]; then
  DIR__="/opt/git-branching-model/Scripts/Feature/"
fi
source $DIR__../Common/variables.sh
if [ $# -ne 1 ]
then
  echo -e "${B_RED}Illegal number of parameters. 1: feature name without spaces.${RESET}"
  exit 1
else
  git $FLAGS status > /dev/null 2>/dev/null
  if [ $? -ne 0 ]; then
    echo -e "${B_YELLOW}Git was not initializaded. ${B_CYAN}Starting git.${RESET}"
    git $FLAGS init > /dev/null 2>/dev/null
  fi
  if [ $(git $FLAGS branch | egrep "(* )master" | wc -l) == 0 ];then #If there is not develop branch, create it.
    touch CHANGELOG.md
    git $FLAGS add CHANGELOG.md
    git $FLAGS commit -m "Inicial Commit." > /dev/null 2>/dev/null
  fi  
  if [ $(git $FLAGS branch | egrep "(* )develop" | wc -l) == 0 ];then #If there is not develop branch, create it.
    git $FLAGS checkout -b develop master      
  fi  
  if [ $(git $FLAGS branch | egrep "(* )feature-$1" | wc -l) == 0 ];then #If the branch does not exits, create it.
    git $FLAGS checkout -b feature-$1 develop
  else
    echo -e "${B_YELLOW}Branch ${B_CYAN}feature-$1 ${B_YELLOW}already exists.${RESET}"
  fi  
fi