#!/usr/bin/env bash
DIR__=""
if [ ! -d ../Hotfix ]; then
  DIR__="/opt/git-branching-model/Scripts/HotFix/"
fi
source $DIR__../Common/variables.sh
if [ -f VERSION ]; then
  VERSION=`cat VERSION`
  PUSHING_MSG="${NOTICE_FLAG} Pushing version "$VERSION" to the ${B_WHITE}origin${B_CYAN}..."  
  if [ "$(git $FLAGS tag --list | grep -q $VERSION)" ]; then #If the version is already tagged.
    echo -e "${RED}ERROR: Version number has been previously created.${RESET}"
  else
    git $FLAGS add CHANGELOG.md VERSION
    git $FLAGS commit -m "Bump version to ${VERSION}."
    git $FLAGS tag -a -m "Tag version ${VERSION}." "v$VERSION"
    if [ $(git remote -v | wc -l) != 0 ] ; then
      git $FLAGS push origin --tags
    fi
    git $FLAGS checkout master
    if [ $(git remote -v | wc -l) != 0 ] ; then
      echo -e "$PUSHING_MSG"
      git $FLAGS push origin
    fi
    git $FLAGS merge --no-ff hotfix-$VERSION -m "Merge branch hotfix-$VERSION into master"
    git checkout develop
    git $FLAGS merge --no-ff hotfix-$VERSION -m "Merge branch hotfix-$VERSION into develop"
    git $FLAGS branch -d hotfix-$VERSION
    if [ $(git remote -v | wc -l) != 0 ] ; then
      echo -e "$PUSHING_MSG"
      git $FLAGS push origin
    fi
  fi
else
  echo -e "${RED}ERROR: Version file not found. Execute new_release.sh before.${RESET}"
fi
