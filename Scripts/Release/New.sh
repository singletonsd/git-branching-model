#!/usr/bin/bash
DIR__=""
if [ ! -d ../Feature ]; then
  DIR__="/opt/git-branching-model/Scripts/Feature/"
fi
source $DIR__../Common/variables.sh
source $DIR__../Version/bump-version.sh -m
if [ -f "$__PATH"/VERSION ]; then
  git $FLAGS checkout -b release-"$(cat "$__PATH"/VERSION)" develop
fi